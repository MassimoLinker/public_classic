local addon, dark_addon = ...

local InstantHealth = LibStub("LibInstantHealth-1.0")
local frame = CreateFrame("Frame")
local function onEvent(self, event, ...)
  
  if event == "UNIT_HEALTH" or event == "UNIT_HEALTH_FREQUENT" then
    local unit = ...
    --    print("UnitHealth(%q) = %d", unit, InstantHealth.UnitHealth(unit))
  elseif event == "UNIT_MAXHEALTH" then
    local unit = ...
    -- print("UnitHealthMax(%q) = %d", unit, InstantHealth.UnitHealthMax(unit))
  end
end
InstantHealth.RegisterCallback(frame, "UNIT_HEALTH", onEvent, frame)
InstantHealth.RegisterCallback(frame, "UNIT_HEALTH_FREQUENT", onEvent, frame)
InstantHealth.RegisterCallback(frame, "UNIT_MAXHEALTH", onEvent, frame)

local health = { }

function health:UNIT_HEALTH(event, unit)
end
function health:UNIT_MAXHEALTH(event, unit)
end

InstantHealth.RegisterCallback(health, "UNIT_HEALTH")
InstantHealth.RegisterCallback(health, "UNIT_HEALTH_FREQUENT", "UNIT_HEALTH") -- redirect "UNIT_HEALTH_FREQUENT" to "UNIT_HEALTH" handler
InstantHealth.RegisterCallback(health, "UNIT_MAXHEALTH")


local UnitHealth = dark_addon.environment.UnitHealth

function health:percent()
  return InstantHealth.UnitHealth(self.unitID) / InstantHealth.UnitHealthMax(self.unitID) * 100
end

function health:actual()
  return InstantHealth.UnitHealth(self.unitID)
end

function health:effective()
  return (InstantHealth.UnitHealth(self.unitID) + (UnitGetIncomingHeals(self.unitID) or 0)) / InstantHealth.UnitHealthMax(self.unitID) * 100
end

function health:missing()
  return InstantHealth.UnitHealthMax(self.unitID) - UnitHealth(self.unitID)
end

function health:max()
  return InstantHealth.UnitHealthMax(self.unitID)
end

function dark_addon.environment.conditions.health(unit, called)
  return setmetatable({
    unitID = unit.unitID
  }, {
    __index = function(t, k)
      return health[k](t)
    end,
    __unm = function(t)
      return health['percent'](t)
    end
  })
end
