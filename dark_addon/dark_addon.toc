## Interface: 11302
## Title: |cffebdec2Addon|r
## Notes: An addon.
## SavedVariablesPerCharacter: dark_addon_storage
## LoadOnDemand: 0
## DefaultState: enabled
## Version: 1.0.0
libs\LibStub-1.0\LibStub.lua
libs\CallbackHandler-1.0\CallbackHandler-1.0.xml
libs\LibSharedMedia-3.0\LibSharedMedia-3.0.lua
libs\LibRangeCheck-2.0\LibRangeCheck-2.0.lua
libs\DiesalTools-1.0\DiesalTools-1.0.lua
libs\DiesalStyle-1.0\DiesalStyle-1.0.lua
libs\DiesalGUI-1.0\DiesalGUI-1.0.xml
libs\DiesalMenu-1.0\DiesalMenu-1.0.xml
libs\LibDispellable-1.0-bfa\LibDispellable-1.0.lua
libs\LibClassicCasterino\LibClassicCasterino.lua
libs\LibDraw-1.0\LibDraw.lua
libs\LibAddonCompat-1.0\LibAddonCompat-1.0.lua
libs\LibBossIDs-1.0\LibBossIDs-1.0.lua
libs\json\json.lua
libs\LibInstantHealth-1.0\LibInstantHealth-1.0.lua
libs\libCh0tFqRg\libCh0tFqRg.lua
libs\libNekSv2Ip\libNekSv2Ip.lua
libs/LibClassicSpecs/LibClassicSpecs.lua

libs/Ace3/AceAddon-3.0\AceAddon-3.0.xml
libs/Ace3/AceEvent-3.0\AceEvent-3.0.xml
libs/Ace3/AceTimer-3.0\AceTimer-3.0.xml
libs/Ace3/AceBucket-3.0\AceBucket-3.0.xml
libs/Ace3/AceHook-3.0\AceHook-3.0.xml
libs/Ace3/AceDB-3.0\AceDB-3.0.xml
libs/Ace3/AceDBOptions-3.0\AceDBOptions-3.0.xml
libs/Ace3/AceLocale-3.0\AceLocale-3.0.xml
libs/Ace3/AceConsole-3.0\AceConsole-3.0.xml
libs/Ace3/AceGUI-3.0\AceGUI-3.0.xml
libs/Ace3/AceConfig-3.0\AceConfig-3.0.xml
libs/Ace3/AceComm-3.0\AceComm-3.0.xml
libs/Ace3/AceTab-3.0\AceTab-3.0.xml
libs/Ace3/AceSerializer-3.0\AceSerializer-3.0.xml
libs/Ace3/Ace3.lua

dark_addon.xml